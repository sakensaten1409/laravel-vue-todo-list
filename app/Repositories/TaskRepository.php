<?php

namespace App\Repositories;

use App\Models\Item;

class TaskRepository
{
    public function getAllTask()
    {
        return Item::query()->orderBy('created_at', 'DESC')->get();
    }

    public function store($data)
    {
        $newTask = new Item;
        $newTask->name = $data['name'];
        $newTask->completed = $data['completed'] ?? false;
        $newTask->completed_at = isset($data['completed_at']) ? date('Y-m-d H:i:s', strtotime($data['completed_at'])) : null;

        return $newTask->save();
    }

    public function update($data)
    {
        $task = Item::find($data['id']);
        $task->name = $data['name'] ?? $task->name;
        $task->completed = $data['completed'] ?? $task->completed;
        $task->completed_at = isset($data['completed_at']) ? date('Y-m-d H:i:s', strtotime($data['completed_at'])) : $task->completed_at;

        return $task->save();
    }

    public function destroy($id)
    {
        return Item::query()->find($id)->delete();
    }
}
