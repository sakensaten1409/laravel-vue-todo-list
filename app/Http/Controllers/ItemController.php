<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskDeleteRequest;
use App\Http\Requests\TaskUpdateRequest;
use App\Repositories\TaskRepository;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\TaskStoreRequest;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param TaskRepository $taskRepository
     * @return JsonResponse
     */
    public function index(TaskRepository $taskRepository): JsonResponse
    {
        $taskRes = $taskRepository->getAllTask();
        return response()->json([
            'success' => !empty($taskRes),
            'data' => $taskRes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskStoreRequest $request
     * @param TaskRepository $taskRepository
     * @return JsonResponse
     */
    public function store(TaskStoreRequest $request, TaskRepository $taskRepository): JsonResponse
    {
        $validated = $request->validated();
        $storeRes = $taskRepository->store($validated);

        if ($storeRes)
            return response()->json([
                'success' => true,
                'message' => 'Task created successfully'
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => $storeRes
            ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskUpdateRequest $request
     * @param TaskRepository $taskRepository
     * @return string
     */
    public function update(TaskUpdateRequest $request, TaskRepository $taskRepository): JsonResponse
    {
        $validated = $request->validated();
        $updRes = $taskRepository->update($validated);

        if ($updRes)
            return response()->json([
                'success' => true,
                'message' => 'Task updated successfully'
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => $updRes
            ], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TaskDeleteRequest $request
     * @param TaskRepository $taskRepository
     * @return string
     */
    public function destroy(TaskDeleteRequest $request, TaskRepository $taskRepository): JsonResponse
    {
        $validated = $request->validated();
        $delRes = $taskRepository->destroy($validated['id']);

        if ($delRes)
            return response()->json([
                'success' => true,
                'message' => 'Task deleted successfully'
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => $delRes
            ], 422);
    }
}
