<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'id' => 'required|exists:items,id|int',
            'name' => 'string|max:255|nullable',
            'completed' => 'boolean|nullable',
            'completed_at' => 'date|after_or_equal:now|date_format:"d.m.Y H:i:s","d.m.Y"|nullable'
        ];
    }
}
