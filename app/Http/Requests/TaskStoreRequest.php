<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'completed' => 'boolean',
            'completed_at' => 'date|after_or_equal:now|date_format:d.m.Y'
        ];
    }
}
