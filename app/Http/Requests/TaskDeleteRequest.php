<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskDeleteRequest extends FormRequest
{
    public function rules()
    {
        return [
            'id' => 'required|exists:items,id|int'
        ];
    }
}
