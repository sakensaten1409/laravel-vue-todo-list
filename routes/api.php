<?php

use App\Http\Controllers\ItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/item')->group(function() {
    Route::get('', [ItemController::class, 'index']);
    Route::post('/store', [ItemController::class, 'store']);
    Route::post('/update', [ItemController::class, 'update']);
    Route::post('/delete', [ItemController::class, 'destroy']);
});
